import React, {useRef, useEffect} from 'react'
import usePaste from './hooks/usePaste';
// import PasteImage from './components/paste_image/paste_image';

function App() {
  let element = useRef(null);

  const image = usePaste(element ? element.current : document);

  useEffect(()=>{
    console.log(image);
  },[image])

  return (
    <div>
      <div className="App" ref={element}>
          PasteImage Here
      </div>
      {/* <PasteImage element={element}/> */}
    </div>
  );
}

export default App;
