import { useState, useEffect } from 'react'

const usePaste = element =>{

    const [localImage, setImage] = useState({blob: null, url: null});

    const getRef = ()=>{
        return element || document;
    }

    const getPlatformType = ()=>{
        const platform = navigator.platform
        if (platform.includes('Win')) {
            return 'windows'
        } else if (platform.includes('Linux')) {
            return 'linux'
        } else if (platform.includes('Mac')) {
            return 'mac'
        } else {
            return false
        }
    }

    const getFiles = (event)=>{
        return (event.clipboardData || event.originalEvent.clipboardData).files;
    }

    // const getItems = (event)=>{
    //     return (event.clipboardData || event.originalEvent.clipboardData).items;
    // }

    const getText = (event)=>{
        // get text representation of clipboard
        return (event.originalEvent || event).clipboardData.getData('text/plain');
    }

    const getPaths = (text)=>{
        const splittedText = text.split('\n');
        const paths = splittedText.filter(line => line.startsWith('file://'))
        return paths;
    }

    const getFileFromPath = (filePath)=>{
        var xhr = new XMLHttpRequest();
        xhr.open("GET", filePath, true);
        xhr.responseType = 'blob';
        xhr.onload = () => {
            if (xhr.status === 200) {
                saveImages(xhr.response, '');
            } else {
                alert('Request failed. Returned status of ' + xhr.status);
            }
        }
        xhr.send()
    }

    const saveImages = (blob, url)=>{
        setImage({ blob, url })
    }

    const setImagesToState = blob => {
        let blobAndUrl = { blob: blob, url: null }

        var reader = new FileReader();
        reader.onload = (event) => {
            blobAndUrl.url = event.target.result;

            setImage(blobAndUrl)
        }; // data url!
        reader.readAsDataURL(blob);
    }

    const getWindowsImages = event => {
        const files = getFiles(event);

        for (var index in files) {
                // console.log(item)
                var item = files[index];
                if (item.kind === 'file') {
                    setImagesToState(item);
                }
            }
    }

    const getLinuxImages = async (event) => {

        const files = getFiles(event);

        if(files.length === 0){
            console.log('linux clipboard not supperted yet')
            const paths = getPaths(getText(event));
            // let paths = [];
            // if (navigator.clipboard) {
            //     const text = await navigator.clipboard.readText();
            //     paths = getPaths(text);
            // }

            if (paths.length > 0) {
                getFileFromPath(paths[0]);
            }
        }else{
            for (var index in files) {
                var item = files[index];
                // console.log(item)
                if (item instanceof Blob){
                    if (item.type.includes('image/')) {
                        setImagesToState(item);
                    }
                }
            }
        }

        // let text;
        // if (navigator.clipboard) {
        //     text = await navigator.clipboard.readText();
        // }
        // else {
        //     text = event.clipboardData.getData('text/plain');
        // }
        // console.log('Got pasted text: ', text);
    }

    useEffect(()=>{
        const ref = getRef();
        ref.onpaste = (event) => {
            console.log('paste')
            // var items = getItems(event);
            // var files = getFiles(event);
            // const paths = getPaths(getText(event));
            // if (paths.length > 0) {
            //     getFileFromPath(paths[0]);
            // }
            // console.log(items)
            // console.log(files)
            // // console.log(JSON.stringify(items)); // will give you the mime types
            // for (var index in items) {
            //     console.log(item)
            //     var item = items[index];
            //     if (item.kind === 'file') {
            //         let blobAndUrl = {blob: null, url: null}
            //         var blob = item.getAsFile();
            //         blobAndUrl.blob = blob
            //         var reader = new FileReader();
            //         reader.onload = (event) => {
            //             // console.log(event.target.result)
            //             blobAndUrl.url = event.target.result;

            //             setState({image: blobAndUrl})
            //         }; // data url!
            //         reader.readAsDataURL(blob);
            //     }
            // }

            if(getPlatformType()==="windows"){
                getWindowsImages(event);
            }
            switch(getPlatformType()){
                case 'windows': 
                    getWindowsImages(event);
                    break;
                case 'linux':
                    getLinuxImages(event);
                    break;
                default: 
                    console.log('Platform belum didukung.')
            }
        }
        // eslint-disable-next-line
    },[]);

    return localImage;
}


export default usePaste;