import React from 'react'

class PasteImage extends React.Component{
    constructor(){
        super();
        this.getRef = this.getRef.bind(this);
        this.getFileFromPath = this.getFileFromPath.bind(this);
        this.saveImages = this.saveImages.bind(this);
    }
    state = {
        image: null,
    }

    getRef(){
        return this.props.element || document;
    }

    getPlatformType(){
        const platform = navigator.platform
        if(platform.includes('Win')){
            return 'windows'
        } else if (platform.includes('Linux')) {
            return 'linux'
        } else if (platform.includes('Mac')) {
            return 'mac'
        }else{
            return false
        }
    }

    getFiles(event){
        return (event.clipboardData || event.originalEvent.clipboardData).files;
    }

    getItems(event){
        return (event.clipboardData || event.originalEvent.clipboardData).items;
    }

    getText(event){
        // get text representation of clipboard
        return (event.originalEvent || event).clipboardData.getData('text/plain');
    }

    getPaths(text){
        const splittedText = text.split('\n');
        const paths = splittedText.filter(line=>line.startsWith('file://'))
        return paths;
    }

    getFileFromPath(filePath){
        var xhr = new XMLHttpRequest();
        xhr.open("GET", filePath, true);
        xhr.responseType = 'blob';
        xhr.onload = () => {
            if (xhr.status === 200) {
                this.saveImages(xhr.response, '');
            }else {
                alert('Request failed. Returned status of ' + xhr.status);
            }
        }
        xhr.send()
    }

    saveImages(blob, url){
        this.setState({blob, url})
    }

    componentDidMount(){
        const ref = this.getRef();
        ref.onpaste = (event) => {
            console.log('paste')
            // var items = this.getItems(event);
            // var files = this.getFiles(event);
            const paths = this.getPaths(this.getText(event));
            if(paths.length>0){
                this.getFileFromPath(paths[0]);
            }
            // console.log(items)
            // console.log(files)
            // // console.log(JSON.stringify(items)); // will give you the mime types
            // for (var index in items) {
            //     console.log(item)
            //     var item = items[index];
            //     if (item.kind === 'file') {
            //         let blobAndUrl = {blob: null, url: null}
            //         var blob = item.getAsFile();
            //         blobAndUrl.blob = blob
            //         var reader = new FileReader();
            //         reader.onload = (event) => {
            //             // console.log(event.target.result)
            //             blobAndUrl.url = event.target.result;

            //             this.setState({image: blobAndUrl})
            //         }; // data url!
            //         reader.readAsDataURL(blob);
            //     }
            // }
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(prevState.image !== this.state.image){
            console.log(this.state.image);
        }
    }

    render(){
        return <div>PasteImage</div>
    }
}

export default PasteImage;